abstract class ItemDownloadSuccessfulNotification {
  Future<void> notify();
}
