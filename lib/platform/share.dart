abstract class Share {
  Future<void> share();
}
