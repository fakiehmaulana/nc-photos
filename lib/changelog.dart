const contents = [
  // v1
  null,
  // v2
  null,
  // v3
  null,
  // v4
  null,
  // v5
  null,
  // v6
  null,
  // v7
  """1.7.0
Added HEIC support
Fixed a bug that corrupted the albums. Please re-add the photos after upgrading. Sorry for your inconvenience
""",
  // v8
  """1.8.0
Dark theme
""",
  // v9
  null,
  // v10
  null,
  // v11
  null,
  // v12
  null,
  // v13
  """13.0
Added MP4 support (Android only)
""",
  // v14
  null,
  // v15
  """15.0
This version includes changes that are not compatible with older versions. Please also update your other devices if applicable
""",
  // v16
  null,
  // v17
  """17.0
Archive photos to only show them in albums
Link to report issues in Settings
""",
  // v18
  """18.0
Modify date/time of photos
Support GIF
""",
  // v19
  """19.0
- Folder based album to browse photos in an existing folder (read only)
- Batch import folder based albums

This version includes changes that are not compatible with older versions. Please also update your other devices if applicable
""",
  // v20
  """20.0
- Improved albums: sorting, text labels
- Simplify sharing to other apps
- Added WebM support (Android only)
""",
  // v21
  null,
  // v22
  null,
  // v23
  """23.0
- Paid version is now published on Play Store. Head to Settings to learn more if you are interested
""",
  // v24
  """24.0
- Show and manage deleted files in trash bin
""",
  // v25
  null,
  // v26
  """26.0
- Pick album cover (open a photo in an album -> details -> use as cover)
""",
  // v27
  """27.0
- New settings to customize photo viewer
""",
];
