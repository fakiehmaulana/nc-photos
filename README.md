# Photos (for Nextcloud)
Browse your photos on Nextcloud servers

[<img src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png" alt="Google Play" width="160" />](https://play.google.com/store/apps/details?id=com.nkming.nc_photos.paid&referrer=utm_source%3Drepo)  
Or the [free ad-supported version](https://play.google.com/store/apps/details?id=com.nkming.nc_photos&referrer=utm_source%3Drepo)

*\*Read [this guide](https://gitlab.com/nkming2/nc-photos/-/wikis/Help/Web-App) if you want to try the experimental web app*

Features:
- Support JPEG, PNG, WebP, HEIC, GIF images
- Support MP4, WebM videos (codec support may vary between devices/browsers)
- EXIF support (JPEG and HEIC only)
- Organize photos with albums that are independent of your file hierarchy
- Sign-in to multiple servers
- and more to come!

Translations (sorted by ISO name):
- français (contributed by mgreil)
- ελληνικά (contributed by Chris Karasoulis)
- русский (contributed by meixnt & eriane)
- Español (contributed by luckkmaxx)

This app does not require any server-side plugins.

Please visit the [wiki](https://gitlab.com/nkming2/nc-photos/-/wikis/home) for more helps and guides
